angular.module('starter.services', [])

  .factory("$fileFactory", function ($q) {

    var File = function () { };

    File.prototype = {

      getParentDirectory: function (path) {
        var deferred = $q.defer();
        window.resolveLocalFileSystemURI(path, function (fileSystem) {
          fileSystem.getParent(function (result) {
            deferred.resolve(result);
          }, function (error) {
            deferred.reject(error);
          });
        }, function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      },

      getEntriesAtRoot: function () {
        var deferred = $q.defer();
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
          var directoryReader = fileSystem.root.createReader();
          directoryReader.readEntries(function (entries) {
            deferred.resolve(entries);
          }, function (error) {
            deferred.reject(error);
          });
        }, function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      },

      getEntries: function (path) {
        var deferred = $q.defer();
        window.resolveLocalFileSystemURI(path, function (fileSystem) {
          var directoryReader = fileSystem.createReader();
          directoryReader.readEntries(function (entries) {
            deferred.resolve(entries);
          }, function (error) {
            deferred.reject(error);
          });
        }, function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      }

    };

    return File;

  })

  .factory('Chats', function () {
    // Might use a resource here that returns a JSON array

    // Some fake testing data
    var chats = [{
      id: 0,
      name: 'Ben Sparrow',
      lastText: 'You on your way?',
      face: 'img/ben.png'
    }, {
        id: 1,
        name: 'Max Lynx',
        lastText: 'Hey, it\'s me',
        face: 'img/max.png'
      }, {
        id: 2,
        name: 'Adam Bradleyson',
        lastText: 'I should buy a boat',
        face: 'img/adam.jpg'
      }, {
        id: 3,
        name: 'Perry Governor',
        lastText: 'Look at my mukluks!',
        face: 'img/perry.png'
      }, {
        id: 4,
        name: 'Mike Harrington',
        lastText: 'This is wicked good ice cream.',
        face: 'img/mike.png'
      }];

    return {
      all: function () {
        return chats;
      },
      remove: function (chat) {
        chats.splice(chats.indexOf(chat), 1);
      },
      get: function (chatId) {
        for (var i = 0; i < chats.length; i++) {
          if (chats[i].id === parseInt(chatId)) {
            return chats[i];
          }
        }
        return null;
      }
    };
  });
