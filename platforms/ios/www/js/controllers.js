angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope) { })

  .controller('ChatsCtrl', function ($scope, Chats, $cordovaFile, $fileFactory) {



    var fs = new $fileFactory();



    document.addEventListener('deviceready', function () {


     fs.getEntriesAtRoot().then(function(result) {
            $scope.files = result;
        }, function(error) {
            console.error(error);
        });

      // CHECK
      $cordovaFile.checkDir('/', "/")
        .then(function (success) {
          // success
          console.log("Accessing file system");
          console.log(success);
        }, function (error) {
          // error
        });



      $scope.chats = Chats.all();
      $scope.remove = function (chat) {
        Chats.remove(chat);
      };

    });

  })

  .controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
  })

  .controller('AccountCtrl', function ($scope) {
    $scope.settings = {
      enableFriends: true
    };
  });
